# PHMI-lab6

### Project Name: 
LinkedOut
### Description: 
Соцсеть для поиска развития нетворкинга, поиска работы и развития. \
Реализует ленту с контентом, встроенный мессенджер, систему связей пользователей. \
Доступно на веб и мобильной версиях.
### Installation: 
Последовательность шагов, как установить приложение
локально.
### Sub modules: 
Ссылки на репозитории для веб-приложения, мобильного
приложения с их кратким описанием.
### Usage:
Рекомендации как использовать приложение после установки.
Может содержать скриншоты.
### Contributing:
Антонов Антон - DevOps Engineer \
Дарья Шевченок - Frontend Dev, TeamLead \
Борисенко Тимур - Backend Dev, Mobile Dev
